#!/usr/bin/env python3

import socket
import os
import json

HOST, PORT = '0.0.0.0', 80

#beruh momentum

wkParts = None
wkId = 0

#Init socket
listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
listen_socket.bind((HOST, PORT))
listen_socket.listen(1)
print ('Serving HTTP on port %s ...' % PORT)
while True:
	#Get client request
	client_connection, client_address = listen_socket.accept()
	request = client_connection.recv(4096).decode('utf-8')
	#Check if request is more than call size
	tokenLines = request.replace("\r", "").split("\n")
	if len(tokenLines) > 1:
		startTokens = tokenLines[0].split()
		#Obtenir header parameters
		headerParams = {};
		headerSize = len(tokenLines[1]);
		for line in tokenLines[1:]:
			if len(line) == 0:
				break
			headerSize += len(line)
			lineTokens = [line[:line.find(":")], line[line.find(":")+1:]]
			headerParams[lineTokens[0].strip()] = lineTokens[1].strip()
			#Recieve more data if not enough
			if "Content-Length" in headerParams and 4096 < int(headerParams["Content-Length"]) + headerSize:
				extraBuff = client_connection.recv(int(headerParams["Content-Length"]) + headerSize - 4096).decode('utf-8')
				request = request + extraBuff
	print(request)
	
	#Handle request
	if len(tokenLines) > 1:
		#If empty resource, send index.html
		if startTokens[1] == "/" or startTokens[1].find("?") == 1:
			params = "";
			if startTokens[1].find("?"):
				params = startTokens[1][1:]
			startTokens[1] = "/index.html" + params
			
		#If contains HTTP parameters
		httpParams = {};
		if  "?" in startTokens[1]:
			params = startTokens[1][startTokens[1].find("?") + 1:].split("&")
			for param in params:
				keyPair = param.split("=")
				httpParams[keyPair[0]] = keyPair[1]
		
		#Handle GET commands
		if startTokens[0] == "GET":
			headerStart = "/HTTP/1.1 200 OK\r\n"
			fileName = startTokens[1]
			if "?" in startTokens[1]:
				fileName = startTokens[1][:startTokens[1].find("?")]
			acceptList = headerParams["Accept"].split(",")
			#If accepting text/html file
			if acceptList[0] == "text/html":
				try:
					file = open(os.path.dirname(os.path.abspath(__file__)) + fileName, 'r')
					client_connection.sendall((headerStart + "\r\n" +  file.read()).encode('utf-8'))
					file.close()
				except:
					print ("Error: An exception has occured when attempting to access " +  fileName + ".")
			#If accepting picture file
			elif acceptList[0] == "image/webp":
				try:
					file = open(os.path.dirname(os.path.abspath(__file__)) + fileName, 'rb')
					fileBytes = file.read()
					contentHeader = "Content-Type: image/gif\r\nContent-Length: " + str(len(fileBytes)) + "\r\n"
					client_connection.sendall((headerStart + contentHeader + "\r\n").encode('utf-8') + fileBytes)
					file.close()
				except:
					print ("Error: An exception has occured when attempting to access " + fileName + ".")
			#If accepting any file
			elif acceptList[0] == "*/*":
				try:
					file = open(os.path.dirname(os.path.abspath(__file__)) + fileName, 'r', encoding="utf-8")
					fileContents = file.read()
					file.close()
					#If file requested is workspace
					msg = None
					if fileName == "/programs/file.wkspace" or fileName == "/programs/default_ps3.wkspace":
						#Send # of parts
						if httpParams["part"] == str(-1):
							msgCount = int((len(fileContents) - 1) / 1024)
							client_connection.sendall((str(msgCount+ 1)).encode("utf-8"))
						#Send last part
						elif httpParams["part"] == str(int((len(fileContents) - 1) / 1024)):	
							msg = headerStart + "\r\n" + str(len(httpParams["part"])) + httpParams["part"] + fileContents[-int(len(fileContents) % 1024):]
							client_connection.sendall(msg.encode("utf-8"))
						#Send other parts
						else:
							msg = headerStart + "\r\n" + str(len(httpParams["part"])) + httpParams["part"] + fileContents[1024 * int(httpParams["part"]):1024 * (int(httpParams["part"]) + 1)]
							client_connection.sendall(msg.encode("utf-8"))
					else:
						msg = headerStart  + "\r\n" + fileContents
						client_connection.sendall(msg.encode("utf-8"))
				except:
					print ("Error: An exception has occured when attempting to access " + fileName+ ".")
		#Handle POST commands
		elif startTokens[0] == "POST":
			#Check if conent type is application/json
			if headerParams["Content-Type"] == "application/json":
				fileContents = json.loads(request[-int(headerParams["Content-Length"]):])["data"]
				try:
					#Check if saving script
					if httpParams["path"] == "/programs/file.py":
						fileContents = "#!/usr/bin/env python3\n\rimport evdev\n\rimport ev3dev.ev3 as ev3dev\n\r" + fileContents
					#Check if saving workspace
					if httpParams["path"] == "/programs/file.wkspace":
						if httpParams["wkid"] != wkId:
							wkId = httpParams["wkid"]
							wkParts = [None] * int(httpParams["partcount"])
						wkParts[int(httpParams["part"])] = fileContents
						if None not in wkParts:
							file = open(os.path.dirname(os.path.abspath(__file__)) + httpParams["path"], 'w+')
							for part in wkParts:
								file.write(part)
							file.close()
					else:
						file = open(os.path.dirname(os.path.abspath(__file__)) + httpParams["path"], 'w+')
						file.write(fileContents)
						file.close()
				except:
					print ("Error: An exception has occured when attempting to access " + os.path.dirname(__file__) + httpParams["path"] + ".")
			elif False:
				pass
	client_connection.close()