Blockly.defineBlocksWithJsonArray([{
  "type": "getlargemotor",
  "message0": "Large motor %1 %2",
  "args0": [
    {
      "type": "field_dropdown",
      "name": "PortNumber",
        "options": [
        [
          "A",
          "ev3dev.OUTPUT_A"
        ],
        [
          "B",
          "ev3dev.OUTPUT_B"
        ],
        [
          "C",
          "ev3dev.OUTPUT_C"
        ],
        [
          "D",
          "ev3dev.OUTPUT_D"
        ]
      ]
    },
    {
      "type": "input_value",
      "name": "Port",
      "check": "Port"
    }
  ],
  "output": "Large Motor",
  "colour": "#ff5c26",
  "tooltip": "The large motor plugged into the specified port.",
  "helpUrl": ""
},
{
  "type": "setlargemotorspeed",
  "message0": "Set large motor %1 's speed to %2.",
  "args0": [
    {
      "type": "input_value",
      "name": "Large Motor",
      "check": "Large Motor",
      "align": "RIGHT"
    },
    {
      "type": "input_value",
      "name": "Speed",
      "check": "Number",
      "align": "RIGHT"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": "#ff5c26",
  "tooltip": "Sets large motor's speed (-1.0 to 1.0).",
  "helpUrl": ""
}
])