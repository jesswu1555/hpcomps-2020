#!/usr/bin/env python3

import evdev
import ev3dev.ev3 as ev3dev
import threading
import file

#Each input contains a tuple of (value, keyCode, eventCode)
class Gamepad:
	def __init__(self):
		#Digital Input
		self.up = [False, 292, 1]
		self.down = [False, 294, 1]
		self.left = [False, 295, 1]
		self.right = [False, 293, 1]
		self.select = [False, 288, 1]
		self.start = [False, 291, 1]
		self.lshoulder = [False, 298, 1]
		self.rshoulder = [False, 299, 1]
		self.triangle =[False, 300, 1]
		self.circle = [False, 301, 1]
		self.x = [False, 302, 1]
		self.square = [False, 303, 1]
		#Analog Input
		self.leftX = [0.0, 0, 3]
		self.leftY = [0.0, 1, 3]
		self.rightX = [0.0, 2, 3]
		self.rightY = [0.0, 5, 3]
		self.ltrigger = [0.0, 48, 3]
		self.rtrigger = [0.0, 49, 3]
		
#Helps with event checking
class GamepadHandler:
	def __init__(self, gamepad):
		self.gamepad = gamepad
		self.keyCodes = [[0, 0, 0] for i in range(512)];
		inputs = gamepad.__dict__
		for input in inputs:
			#Generate keyCode array
			self.keyCodes[inputs[input][1]] = inputs[input]
			
	def setInput(self, keyCode, value):
		self.keyCodes[keyCode][0] = value
		
gamepad = Gamepad();
gpadHandler = GamepadHandler(gamepad)
		
#Find controller
print("Finding PS3 controller...")
devices = [evdev.InputDevice(fn) for fn in evdev.list_devices()]
for device in devices:
	if device.name == "PLAYSTATION(R)3 Controller":
		ps3Dev = device.fn
		
inputDev = evdev.InputDevice(ps3Dev)

#Robot specific code
class MyCodeThread(threading.Thread):
	def __init__(self):
		threading.Thread.__init__(self)
		
	def run(self):
		#Run robot code file (may change later)
		file.RunRobot(gamepad)

#Start thread
myCodeThread = MyCodeThread()
myCodeThread.setDaemon(True)
myCodeThread.start()

#Update gamepad
for event in inputDev.read_loop():
	gpadHandler.setInput(event.code, event.value)