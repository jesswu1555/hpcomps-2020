#!/usr/bin/env python3
import evdev
import ev3dev.ev3 as ev3dev
gamepad = None
wheelL = None
wheelR = None
maxWheelSpd = None
wheelLSpd = None
wheelRSpd = None

"""The code the robot will run. All the code must go in this block.
"""
def RunRobot(gamepad):
  global wheelL, wheelR, maxWheelSpd, wheelLSpd, wheelRSpd
  # Set "wheelL" to the large motor in port A.
  wheelL = ev3dev.LargeMotor(ev3dev.OUTPUT_A)
  # Set "wheelR" to the large motor in port B.
  wheelR = ev3dev.LargeMotor(ev3dev.OUTPUT_B)
  # Repeat the following commands over and over.
  while True:
    # Set the maximum wheel speed to 0.3.
    maxWheelSpd = 0.3
    # If either shoulder button is held, increase the maximum speed to 0.8.
    if (gamepad.lshoulder[0]) or (gamepad.rshoulder[0]):
      maxWheelSpd = 0.8
    # Set the left wheel speed to 0. If other buttons
    # are pressed, this value will be overwritten.
    wheelLSpd = 0
    # Set the right wheel speed to 0. If other buttons
    # are pressed, this value will be overwritten.
    wheelLSpd = 0
    # If the "up" button is held, set the left and right wheels to go forwards.
    if gamepad.up[0]:
      wheelLSpd = maxWheelSpd * 1
      wheelRSpd = maxWheelSpd * 1
    # If the "down" button is held, set the left and right wheels to go backwards.
    if gamepad.down[0]:
      wheelLSpd = maxWheelSpd * -1
      wheelRSpd = maxWheelSpd * -1
    # If the "left" button is held, set the left wheel to go backwards and
    # the right wheel to go forwards. This spins the robot to the left.
    if gamepad.left[0]:
      wheelLSpd = maxWheelSpd * -1
      wheelRSpd = maxWheelSpd * 1
    # If the "right" button is held, set the left wheel to go forwards and
    # the right wheel to go backwards. This spins the robot to the right.
    if gamepad.right[0]:
      wheelLSpd = maxWheelSpd * 1
      wheelRSpd = maxWheelSpd * -1
    # Set wheelL's motor speed to wheelLSpd.
    wheelL.run_direct(duty_cycle_sp = (wheelLSpd * 1) * 50)
    # Set wheelL's motor speed to wheelLSpd. The speed is
    # multiplied by -1 to account for the motor's orientation.
    wheelR.run_direct(duty_cycle_sp = (wheelRSpd * -1) * 50)
