Blockly.Python['getlargemotor'] = function(block) {
  var dropdown_portnumber = block.getFieldValue('PortNumber');
  var value_port = Blockly.Python.valueToCode(block, 'Port', Blockly.Python.ORDER_ATOMIC);
  var code = 'ev3dev.LargeMotor(' + dropdown_portnumber + ')';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['setlargemotorspeed'] = function(block) {
  var value_largemotor = Blockly.Python.valueToCode(block, 'Large Motor', Blockly.Python.ORDER_ATOMIC);
  var value_speed = Blockly.Python.valueToCode(block, 'Speed', Blockly.Python.ORDER_ATOMIC);
  var code = value_largemotor + '.run_direct(duty_cycle_sp = ' + value_speed  + ' * 50)\n';
  return code;
};