Blockly.defineBlocksWithJsonArray([{
  "type": "start",
  "message0": "Start is held",
  "output": "Boolean",
  "colour": 120,
  "tooltip": "True if \"start\" is being held down.",
  "helpUrl": ""
},
{
  "type": "select",
  "message0": "Select is held",
  "output": "Boolean",
  "colour": 120,
  "tooltip": "True if \"select\" is being held down.",
  "helpUrl": ""
},
{
  "type": "up",
  "message0": "%1 is held",
  "args0" : [
	{
		"type": "field_image",
		"src": "media/up.png",
		"width": 20,
		"height": 20,
		"alt": "Up"
	}
  ],
  "output": "Boolean",
  "colour": 120,
  "tooltip": "True if \"up\" is being held down.",
  "helpUrl": ""
},
{
  "type": "down",
  "message0": "%1 is held",
  "args0" : [
	{
		"type": "field_image",
		"src": "media/down.png",
		"width": 20,
		"height": 20,
		"alt": "Down"
	}
  ],
  "output": "Boolean",
  "colour": 120,
  "tooltip": "True if \"down\" is being held down.",
  "helpUrl": ""
},
{
  "type": "left",
  "message0": "%1 is held",
  "args0" : [
	{
		"type": "field_image",
		"src": "media/left.png",
		"width": 20,
		"height": 20,
		"alt": "Left"
	}
  ],
  "output": "Boolean",
  "colour": 120,
  "tooltip": "True if \"left\" is being held down.",
  "helpUrl": ""
},
{
  "type": "right",
  "message0": "%1 is held",
  "args0" : [
	{
		"type": "field_image",
		"src": "media/right.png",
		"width": 20,
		"height": 20,
		"alt": "Right"
	}
  ],
  "output": "Boolean",
  "colour": 120,
  "tooltip": "True if \"right\" is being held down.",
  "helpUrl": ""
},
{
  "type": "lshoulder",
  "message0": "LeftShoulder is held",
  "output": "Boolean",
  "colour": 120,
  "tooltip": "True if the left shoulder button is being held down.",
  "helpUrl": ""
},
{
  "type": "rshoulder",
  "message0": "RightShoulder is held",
  "output": "Boolean",
  "colour": 120,
  "tooltip": "True if the right shoulder button is being held down.",
  "helpUrl": ""
},
{
  "type": "triangle",
  "message0": "%1 is held",
  "args0" : [
	{
		"type": "field_image",
		"src": "media/triangle.png",
		"width": 20,
		"height": 20,
		"alt": "Triangle"
	}
  ],
  "output": "Boolean",
  "colour": 120,
  "tooltip": "True if \"triangle\" is being held down.",
  "helpUrl": ""
},
{
  "type": "circle",
  "message0": "%1 is held",
  "args0" : [
	{
		"type": "field_image",
		"src": "media/circle.png",
		"width": 20,
		"height": 20,
		"alt": "Circle"
	}
  ],
  "output": "Boolean",
  "colour": 120,
  "tooltip": "True if \"circle\" is being held down.",
  "helpUrl": ""
},
{
  "type": "x",
  "message0": "%1 is held",
  "args0" : [
	{
		"type": "field_image",
		"src": "media/x.png",
		"width": 20,
		"height": 20,
		"alt": "X"
	}
  ],
  "output": "Boolean",
  "colour": 120,
  "tooltip": "True if \"x\" is being held down.",
  "helpUrl": ""
},
{
  "type": "square",
  "message0": "%1 is held",
  "args0" : [
	{
		"type": "field_image",
		"src": "media/square.png",
		"width": 20,
		"height": 20,
		"alt": "Square"
	}
  ],
  "output": "Boolean",
  "colour": 120,
  "tooltip": "True if \"square\" is being held down.",
  "helpUrl": ""
},
{
  "type": "leftstickx",
  "message0": "left stick's x",
  "output": "Number",
  "colour": 120,
  "tooltip": "The left analog stick's x position (-1.0 to 1.0).",
  "helpUrl": ""
},
{
  "type": "leftsticky",
  "message0": "left stick's y",
  "output": "Number",
  "colour": 120,
  "tooltip": "The left analog stick's y position (-1.0 to 1.0).",
  "helpUrl": ""
},
{
  "type": "rightstickx",
  "message0": "right stick's x",
  "output": "Number",
  "colour": 120,
  "tooltip": "The right analog stick's x position (-1.0 to 1.0).",
  "helpUrl": ""
},
{
  "type": "rightsticky",
  "message0": "right stick's y",
  "output": "Number",
  "colour": 120,
  "tooltip": "The right analog stick's y position (-1.0 to 1.0).",
  "helpUrl": ""
},
{
  "type": "ltrigger",
  "message0": "left trigger value",
  "output": "Number",
  "colour": 120,
  "tooltip": "The left trigger's value (0.0 to 1.0).",
  "helpUrl": ""
},
{
  "type": "rtrigger",
  "message0": "right trigger value",
  "output": "Number",
  "colour": 120,
  "tooltip": "The right trigger's value (0.0 to 1.0).",
  "helpUrl": ""
},
])