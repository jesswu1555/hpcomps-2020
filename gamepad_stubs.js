Blockly.Python['start'] = function(block) {
  var code = 'gamepad.start[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['select'] = function(block) {
  var code = 'gamepad.select[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['up'] = function(block) {
  var code = 'gamepad.up[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['down'] = function(block) {
  var code = 'gamepad.down[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['left'] = function(block) {
  var code = 'gamepad.left[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['right'] = function(block) {
  var code = 'gamepad.right[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['lshoulder'] = function(block) {
  var code = 'gamepad.lshoulder[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['rshoulder'] = function(block) {
  var code = 'gamepad.rshoulder[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['triangle'] = function(block) {
  var code = 'gamepad.triangle[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['circle'] = function(block) {
  var code = 'gamepad.circle[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['x'] = function(block) {
  var code = 'gamepad.x[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['square'] = function(block) {
  var code = 'gamepad.square[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['leftstickx'] = function(block) {
  var code = 'gamepad.leftX[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['leftsticky'] = function(block) {
  var code = 'gamepad.leftY[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['rightstickx'] = function(block) {
  var code = 'gamepad.rightX[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['rightsticky'] = function(block) {
  var code = 'gamepad.rightY[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['ltrigger'] = function(block) {
  var code = 'gamepad.ltrigger[0]';
  return [code, Blockly.Python.ORDER_NONE];
};

Blockly.Python['rtrigger'] = function(block) {
  var code = 'gamepad.rtrigger[0]';
  return [code, Blockly.Python.ORDER_NONE];
};